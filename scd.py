import re
import camelot
import openpyxl
import pandas as pd

# Remove non-alphanumeric characters and spaces
def get_data(file_name):
	file_path = input(f"\nEnter the file path for {file_name} Check Name: ")
	file_var = file_path.replace('"','')
	with open(file_var, 'r') as file:
		lines = file.readlines()
		
	# Strip newline characters and whitespace from each line
	org_data = [line.strip() for line in lines]
	
	#Clean Up of Data
	# Remove - (Automated) | (Manual)
	text1 = [element.replace("(Automated)", '').replace("(Manual)", '') for element in org_data]
	text2 = [re.sub(r'^\d+(\.\d+)*\s+', '', element) for element in text1]
	text3 = [re.sub(r'\d+', '', element) for element in text2]
	text4 = [element.lower() for element in text3]
	clean_data = [re.sub(r'\W+', '', element) for element in text4]
	org_data.insert(0, file_name)
	clean_data.insert(0, file_name)
	
	check_names = {k: v for k, v in zip(clean_data, org_data)}
	#print(check_names)
	return check_names
	
def get_path(file_name):
	file_path = input(f"\nEnter the file path for {file_name}: ")
	excel_path = file_path.replace('"', '')
	return excel_path
	
def save_to_excel(excel_path, excel_sheet_name, lis):
	workbook = openpyxl.load_workbook(excel_path)
	new_sheet = workbook.create_sheet(excel_sheet_name)
	for index, value in enumerate(lis, start=1):
		new_sheet.cell(row=index, column=1).value = value
	# Save the workbook with the new sheet
	workbook.save(excel_path)
	
def get_table_from_pdf(file_name1, file_name2):
    pdf_path = get_path(file_name1)
    excel_output_path = get_path(file_name2)

    start_page = int(input("\nEnter the start page number: "))
    end_page = int(input("Enter the end page number: "))

    tables = camelot.read_pdf(pdf_path, pages=f"{start_page}-{end_page}", flavor='stream')
    
    # Excel Operations
    with pd.ExcelWriter(excel_output_path) as writer:
        for i, table in enumerate(tables):
            df = table.df
            df.to_excel(writer, sheet_name=f"Table {i+1}", index=False)	
	
def is_duplicate_key(dictionary, key):
    count = sum(1 for k in dictionary.keys() if k == key)
    return count > 1

def check_duplicate_key(dictionary, key):
    if is_duplicate_key(dictionary, key):
        #print(f"'{key}' is a duplicate key")
        dictionary[key] = [dictionary[key], 'Duplicate Check']
    else:
        pass
        
        
def compare_dict_values(old_dict, new_dict):
	additional_checks = []
	removeable_checks = []
	
	for key in old_dict:
		if key in new_dict:
			value1 = old_dict[key]
			value2 = new_dict[key]
			pass
		else:
			removeable_checks.append(old_dict[key])
			#print(f"Key '{key}' is not present in NEW SCD")
	for key in new_dict:
		if key not in old_dict:
			additional_checks.append(new_dict[key])
			#print(f"Key '{key}' is not present in OLD SCD")
	return additional_checks, removeable_checks
	
            
def remove_exception_checks(check,NA):
	keys_to_remove = []
	for key in check:
		if key in NA:
			keys_to_remove.append(key)
	
	for key in keys_to_remove:
		check.pop(key)


if __name__ == '__main__':
	
	try:
		val = int(input("""\nWhat operation do you want to perform?\n[1] Extract Check Name from PDF\n[2] Compare SCDs\n\tEnter your choice: """))
		
		if val == int(1):
			get_table_from_pdf('PDF File','Excel')
			print("\nDone")
		elif val == int(2):
			
			OLD_SCD = get_data("OLD SCD")
			NEW_SCD = get_data("NEW SCD")
			NA_Exception = get_data("NA / Exception")
			Excel_path = get_path('Excel')
			
			remove_exception_checks(OLD_SCD,NA_Exception)
			remove_exception_checks(NEW_SCD,NA_Exception)
			old_check = list(OLD_SCD.values())
			new_check = list(NEW_SCD.values())
			na_check = list(NA_Exception.values())
			
			additional_checks,removeable_checks = compare_dict_values(OLD_SCD, NEW_SCD)
			
			#Saving Data to Excel
			
			save_to_excel(Excel_path, 'OLD SCD Check Name', old_check)
			save_to_excel(Excel_path, 'NEW SCD Check Name', new_check)
			save_to_excel(Excel_path, 'NA-Exception Check Name', na_check)
			
			save_to_excel(Excel_path, 'Additional Check Name', additional_checks)
			save_to_excel(Excel_path, 'Removable Check Name', removeable_checks)
			
			print("Note : NEW Check List won't contain NA/Exception Check Name")
			print("\nDone")
		else:
			print("Enter valid input")
	except ValueError:
		print("Invalid input! Please enter a valid integer.")
	

#Requirment
#pip install camelot-py
#pip install pandas
#pip install openpyxl
	
