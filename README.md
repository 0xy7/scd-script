# SCD Project Script

## Purpose
This script is created with the intention of easing the project work.

Features:
- Extracting Check Name from pdf
- Compare SCD - Bifurcate the Additional/Removable Checks.

## Installation
Make sure you have installed Python on your system.
```
git clone https://gitlab.com/0xy7/scd-script.git
cd ./SCD-Script
pip3 install -r requirements.txt
```

## Usage
Execute the script:
`python scd.py`

##### To Extract the Check Names from the PDF Table: 
Provide the absolute path of the PDF and Empty Excel files.
Enter the start and end page numbers of the Recommendation Summary Table from the PDF.

##### Bifurcate the Additional/Removable Checks
Provide the absolute path for Old SCD ,NA / Exception SCD and New SCD Check name in text file.
Provide the absolute path for Empty Excel files